FROM archlinux

LABEL maintainer="sandroandrade@ifba.edu.br"

RUN sed -i -e 's/^#Include/Include/g' /etc/pacman.conf && \
    sed -i -e 's/^#\(\[.*\]\)/\1/g' /etc/pacman.conf && \
    sed -i -e 's/^\[testing\]/[kde-unstable]\nInclude = \/etc\/pacman.d\/mirrorlist\n\n[testing]/g' /etc/pacman.conf && \
    sed -i -e 's/^\[custom\]/#\[custom\]/g' /etc/pacman.conf && \
    sed -i -e 's/ usr\/share\/doc\/\*//g' /etc/pacman.conf && \
    echo 'Server = http://archlinux.c3sl.ufpr.br/$repo/os/$arch' > /etc/pacman.d/mirrorlist && \
    pacman -Syuu --noconfirm && \
    pacman -S wget sudo git cmake make ninja gcc fakeroot clang clazy cppcheck python-cpplint python \
              qt5-base \
              qt5-declarative \
              qt5-xmlpatterns \
              --noconfirm && \
    pacman -Sccc --noconfirm

RUN useradd -m qt && echo "qt ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
ENV HOME /home/qt/
USER qt
WORKDIR /home/qt/

RUN wget https://aur.archlinux.org/cgit/aur.git/snapshot/yay-bin.tar.gz && \
    tar -xzvf yay-bin.tar.gz && cd yay-bin && makepkg -si --noconfirm && cd .. && rm -rf yay-bin* && \
    yay -S include-what-you-use --removemake --noconfirm

CMD ["/bin/bash"]
